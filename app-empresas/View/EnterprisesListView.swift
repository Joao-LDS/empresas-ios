//
//  EnterprisesListView.swift
//  app-empresas
//
//  Created by João on 21/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

class EnterprisesListView: UIView {
    
    lazy var logoutButton: FloatButton = {
        let view = FloatButton()
        view.setImage(UIImage(named: "back_arrow"), for: .normal)
        return view
    }()

    lazy var topView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "top_view")
        return view
    }()
    
    lazy var searchTextField: CustomSearchTextField = {
        let view = CustomSearchTextField(placeholder: "Pesquise por empresa")
        return view
    }()
    
    lazy var resultsFoundLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
        view.font = .systemFont(ofSize: 14)
        return view
    }()
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .white
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension EnterprisesListView: ConfigureView {
    func addViews() {
        addSubview(topView)
        addSubview(logoutButton)
        addSubview(searchTextField)
        addSubview(resultsFoundLabel)
        addSubview(tableView)
    }
    
    func addContraints() {
        NSLayoutConstraint.activate([
            logoutButton.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 10),
            logoutButton.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            topView.topAnchor.constraint(equalTo: topAnchor),
            topView.leadingAnchor.constraint(equalTo: leadingAnchor),
            topView.trailingAnchor.constraint(equalTo: trailingAnchor),
            topView.bottomAnchor.constraint(equalTo: searchTextField.centerYAnchor),
            
            searchTextField.topAnchor.constraint(equalTo: logoutButton.bottomAnchor, constant: 10),
            searchTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            searchTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            searchTextField.heightAnchor.constraint(equalToConstant: 50),
            
            resultsFoundLabel.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 16),
            resultsFoundLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            resultsFoundLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
            tableView.topAnchor.constraint(equalTo: resultsFoundLabel.bottomAnchor, constant: 10),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
