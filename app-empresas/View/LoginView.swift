//
//  LoginView.swift
//  app-empresas
//
//  Created by João on 20/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    lazy var header: CustomHeader = {
        let view = CustomHeader()
        view.setTopLabelText("Seja bem vindo ao empresas!")
        return view
    }()
    
    lazy var emailTextField: CustomTextField = {
        let view = CustomTextField(type: .email)
        return view
    }()
    
    lazy var passwordTextField: CustomTextField = {
        let view = CustomTextField(type: .password)
        return view
    }()
    
    lazy var logInButton: CustomButton = {
        let view = CustomButton()
        view.setTitle("ENTRAR", for: .normal)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .white
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCredentialError() {
        emailTextField.configureCredentialError()
        passwordTextField.configureCredentialError()
        passwordTextField.alertLabel.text = "Credenciais incorretas"
    }
}

extension LoginView: ConfigureView {
    func addViews() {
        addSubview(header)
        addSubview(emailTextField)
        addSubview(passwordTextField)
        addSubview(logInButton)
    }
    
    func addContraints() {
        NSLayoutConstraint.activate([
            header.topAnchor.constraint(equalTo: topAnchor),
            header.leadingAnchor.constraint(equalTo: leadingAnchor),
            header.trailingAnchor.constraint(equalTo: trailingAnchor),
            header.heightAnchor.constraint(equalToConstant: ScreeSize.proportionallyHeight(0.3)),
            
            emailTextField.topAnchor.constraint(equalTo: header.bottomAnchor, constant: 30),
            emailTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            emailTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
            passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 16),
            passwordTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            passwordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
            logInButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 40),
            logInButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            logInButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30),
            logInButton.heightAnchor.constraint(equalToConstant: 48)
        ])
    }
}
