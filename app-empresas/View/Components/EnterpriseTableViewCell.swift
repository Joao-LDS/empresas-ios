//
//  EnterpriseTableViewCell.swift
//  app-empresas
//
//  Created by João on 21/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

class EnterpriseTableViewCell: UITableViewCell {
    
    static let identifier = "EnterpriseTableViewCell"
    
    lazy var backImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 0.6915614009, green: 0.1050354466, blue: 0.5562877655, alpha: 0.506635274)
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .white
        view.font = .systemFont(ofSize: 18, weight: .bold)
        view.numberOfLines = 0
        view.textAlignment = .center
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        backgroundColor = .white
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension EnterpriseTableViewCell: ConfigureView {
    func addViews() {
        backImageView.addSubview(nameLabel)
        contentView.addSubview(backImageView)
    }
    
    func addContraints() {
        NSLayoutConstraint.activate([
            backImageView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            backImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            backImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            backImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            
            nameLabel.trailingAnchor.constraint(equalTo: backImageView.trailingAnchor, constant: -16),
            nameLabel.leadingAnchor.constraint(equalTo: backImageView.leadingAnchor, constant: 16),
            nameLabel.bottomAnchor.constraint(equalTo: backImageView.bottomAnchor, constant: -20)
        ])
    }
    
    
}
