//
//  CustomHeader.swift
//  app-empresas
//
//  Created by João on 20/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

class CustomHeader: UIView {
    
    lazy var backgroundImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "header_image")
        return view
    }()

    lazy var logoImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "logo_home")
        return view
    }()
    
    lazy var topLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .systemFont(ofSize: 20)
        view.textColor = .white
        view.numberOfLines = 0
        view.textAlignment = .center
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setTopLabelText(_ text: String) {
        topLabel.text = text
    }

}

extension CustomHeader: ConfigureView {
    func addViews() {
        addSubview(backgroundImageView)
        addSubview(logoImageView)
        addSubview(topLabel)
    }
    
    func addContraints() {
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: topAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            logoImageView.bottomAnchor.constraint(equalTo: topLabel.topAnchor, constant: -17),
            logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            topLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            topLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            topLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30)
        ])
    }
}
