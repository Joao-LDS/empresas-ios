//
//  CustomSearchTextField.swift
//  app-empresas
//
//  Created by João on 21/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

class CustomSearchTextField: UITextField {
    
    private lazy var iconImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "search")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    init(placeholder: String?) {
        super.init(frame: .zero)
        self.placeholder = placeholder
        setupView()
        configureTextField()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureTextField() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        layer.cornerRadius = 4
        leftViewMode = .always
        textColor = .black
        attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
    }
    
}

extension CustomSearchTextField: ConfigureView {
    func addViews() {
        leftView = iconImageView
    }
    
    func addContraints() {
        iconImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: 40).isActive = true
    }
}
