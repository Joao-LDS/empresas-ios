 //
//  CustomButton.swift
//  app-empresas
//
//  Created by João on 20/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .white)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        configureButton()
        setupView()
    }
    
    func configureButton() {
        layer.cornerRadius = 8
        backgroundColor = #colorLiteral(red: 0.8784313725, green: 0.1176470588, blue: 0.4117647059, alpha: 1)
        tintColor = .white
        titleLabel?.font = .systemFont(ofSize: 16, weight: .bold)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

 extension CustomButton: ConfigureView {
    func addViews() {
        addSubview(activityIndicator)
    }
    
    func addContraints() {
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive
         = true
        guard let titleLabel = titleLabel else { return }
        activityIndicator.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 16).isActive = true
    }
 }
