//
//  CustomTextField.swift
//  app-empresas
//
//  Created by João on 20/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

enum TypeOfTextField {
    case email
    case password
}

class CustomTextField: UIView {
    
    lazy var backgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        view.layer.cornerRadius = 4
        return view
    }()
    
    lazy var topLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
        view.font = .systemFont(ofSize: 14)
        return view
    }()
    
    lazy var textField: UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .black
        return view
    }()
    
    lazy var button: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    lazy var alertLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = #colorLiteral(red: 0.8784313725, green: 0, blue: 0, alpha: 1)
        view.font = .systemFont(ofSize: 12)
        return view
    }()

    init(type: TypeOfTextField) {
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        configureTextField(type)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureTextField(_ type: TypeOfTextField) {
        switch type {
        case .email:
            topLabel.text = "Email"
            textField.keyboardType = .emailAddress
        case .password:
            topLabel.text = "Senha"
            button.setImage(UIImage(named: "eye_pass"), for: .normal)
            textField.isSecureTextEntry = true
        }
    }
    
    func configureCredentialError() {
        backgroundView.layer.borderColor = UIColor.red.cgColor
        backgroundView.layer.borderWidth = 1
        button.setImage(UIImage(named: "error"), for: .normal)
        button.isUserInteractionEnabled = false
    }
}

extension CustomTextField: ConfigureView {
    func addViews() {
        addSubview(backgroundView)
        addSubview(topLabel)
        backgroundView.addSubview(textField)
        backgroundView.addSubview(button)
        addSubview(alertLabel)
    }
    
    func addContraints() {
        NSLayoutConstraint.activate([
            topLabel.topAnchor.constraint(equalTo: topAnchor),
            topLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
            
            backgroundView.heightAnchor.constraint(equalToConstant: 48),
            backgroundView.topAnchor.constraint(equalTo: topLabel.bottomAnchor, constant: 6),
            backgroundView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            textField.topAnchor.constraint(equalTo: backgroundView.topAnchor, constant: 12),
            textField.leadingAnchor.constraint(equalTo: backgroundView.leadingAnchor, constant: 12),
            textField.bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor, constant: -12),
            textField.trailingAnchor.constraint(equalTo: button.leadingAnchor, constant: -12),
            
            button.centerYAnchor.constraint(equalTo: backgroundView.centerYAnchor),
            button.trailingAnchor.constraint(equalTo: backgroundView.trailingAnchor, constant: -16),
            button.widthAnchor.constraint(equalToConstant: 22),
            button.heightAnchor.constraint(equalToConstant: 22),
            
            alertLabel.topAnchor.constraint(equalTo: backgroundView.bottomAnchor, constant: 6),
            alertLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            alertLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4),
        ])
    }
}
