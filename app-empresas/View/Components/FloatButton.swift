//
//  FloatButton.swift
//  app-empresas
//
//  Created by João on 21/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

class FloatButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: .zero)
        configureButton()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureButton() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        layer.cornerRadius = 4
    }
}

extension FloatButton: ConfigureView {
    func addViews() { }
    
    func addContraints() {
        heightAnchor.constraint(equalToConstant: 40).isActive = true
        widthAnchor.constraint(equalToConstant: 40).isActive = true
    }
}
