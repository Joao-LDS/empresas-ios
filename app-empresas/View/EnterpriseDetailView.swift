//
//  EnterpriseDetailView.swift
//  app-empresas
//
//  Created by João on 22/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

class EnterpriseDetailView: UIScrollView {
    
    lazy var container: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var backButton: FloatButton = {
        let view = FloatButton()
        view.setImage(UIImage(named: "back_arrow"), for: .normal)
        return view
    }()
    
    lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .black
        view.font = .systemFont(ofSize: 20, weight: .bold)
        view.numberOfLines = 0
        view.textAlignment = .center
        return view
    }()
    
    lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var detailLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
        view.font = .systemFont(ofSize: 18)
        view.numberOfLines = 0
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .white
        setupView()
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension EnterpriseDetailView: ConfigureView {
    func addViews() {
        container.addSubview(backButton)
        container.addSubview(nameLabel)
        container.addSubview(imageView)
        container.addSubview(detailLabel)
        addSubview(container)
    }
    
    func addContraints() {
        NSLayoutConstraint.activate([
            backButton.topAnchor.constraint(equalTo: container.topAnchor, constant: 20),
            backButton.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 16),
            
            nameLabel.centerYAnchor.constraint(equalTo: backButton.centerYAnchor),
            nameLabel.centerXAnchor.constraint(equalTo: container.centerXAnchor),
            nameLabel.leadingAnchor.constraint(greaterThanOrEqualTo: backButton.trailingAnchor, constant: 16),
            
            imageView.topAnchor.constraint(equalTo: backButton.bottomAnchor, constant: 16),
            imageView.widthAnchor.constraint(equalToConstant: ScreeSize.width),
            imageView.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            imageView.heightAnchor.constraint(equalToConstant: ScreeSize.proportionallyHeight(0.2)),
            
            detailLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 24),
            detailLabel.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 16),
            detailLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -16),
            detailLabel.bottomAnchor.constraint(equalTo: container.bottomAnchor),
            
            container.topAnchor.constraint(equalTo: topAnchor),
            container.centerXAnchor.constraint(equalTo: centerXAnchor),
            container.widthAnchor.constraint(equalTo: widthAnchor),
            container.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
