//
//  EnterprisesListViewModel.swift
//  app-empresas
//
//  Created by João on 21/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import Foundation

typealias Enterprise = EnterpriseResponse.Enterprise

protocol EnterprisesListViewModelProtocol {
    var succesGetEnterprises: ((Int) -> Void)? { get set }
    var failedGetEnterprises: ((String) -> Void)? { get set }
    func logout(completion: @escaping(Bool) -> Void)
    var numberOfRowsTableView: Int { get }
    var enterprises: [Enterprise] { get }
    func getEnterprises(_ name: String?)
    func enterprisePhoto(at index: Int) -> URL?
}

class EnterprisesListViewModel: EnterprisesListViewModelProtocol {
    
    var succesGetEnterprises: ((Int) -> Void)?
    var failedGetEnterprises: ((String) -> Void)?
    var enterprises: [Enterprise] = []
    
    var numberOfRowsTableView: Int {
        return enterprises.count
    }
    
    func enterprisePhoto(at index: Int) -> URL? {
        let photo = enterprises[index].photo
        return URL(string: "\(Request.path)\(photo)")
    }
    
    func getEnterprises(_ name: String?) {
        guard let name = name else { return }
        EnterprisesServiceRepository.getEnterprises(name: name) { result in
            switch result {
            case .success(let enterprises):
                self.enterprises = enterprises.enterprises
                self.succesGetEnterprises?(self.enterprises.count)
            case .failure(let error):
                self.failedGetEnterprises?(error.message)
            }
        }
    }
    
    func logout(completion: @escaping(Bool) -> Void) {
        LoginServiceRepository.logout { success in
            completion(success)
        }
    }
}
