//
//  EnterpriseDetailViewModel.swift
//  app-empresas
//
//  Created by João on 22/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import Foundation

protocol EnterpriseDetailViewModelProtocol {
    var enterprise: Enterprise { get }
    func enterprisePhoto() -> URL?
}

class EnterpriseDetailViewModel: EnterpriseDetailViewModelProtocol {
    
    var enterprise: Enterprise
    
    init(enterprise: Enterprise) {
        self.enterprise = enterprise
    }
    
    func enterprisePhoto() -> URL? {
        let photo = enterprise.photo
        return URL(string: "\(Request.path)\(photo)")
    }
}
