//
//  LoginViewModel.swift
//  app-empresas
//
//  Created by João on 20/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import Foundation

protocol LoginViewModelProtocol {
    var email: String? { get set }
    var password: String? { get set }
    var succesLogin: (() -> Void)? { get set }
    var failedLogin: (() -> Void)? { get set }
    func makeLogin()
}

class LoginViewModel: LoginViewModelProtocol {
    var email: String?
    var password: String?
    var succesLogin: (() -> Void)?
    var failedLogin: (() -> Void)?
    
    func makeLogin() {
        guard let email = email,
            let password = password else {
                self.failedLogin?()
                return
        }
        LoginServiceRepository.login(email, password) { result in
            switch result {
            case .success(let headers):
                Cache.saveHeader(headers)
                self.succesLogin?()
            case .failure:
                self.failedLogin?()
            }
        }
    }
}
