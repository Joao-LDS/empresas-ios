//
//  Extension+UIViewController.swift
//  app-empresas
//
//  Created by João on 22/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardOnTapView() {
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
    }
    
    func moveWhenKeyboardAppears() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        self.view.frame.origin.y = -50
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            view.frame.origin.y = 0
        }
    }
    
    @objc private func hideKeyboard() {
        view.endEditing(true)
    }
}
