//
//  ScreeSize.swift
//  app-empresas
//
//  Created by João on 22/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

class ScreeSize {
    static let width = UIScreen.main.bounds.width
    static let height = UIScreen.main.bounds.height
    
    static func proportionallyHeight(_ value: CGFloat) -> CGFloat {
        return height * value
    }
    
    static func proportionallyWidth(_ value: CGFloat) -> CGFloat {
        return width * value
    }
}
