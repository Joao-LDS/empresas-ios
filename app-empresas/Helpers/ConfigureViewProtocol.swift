//
//  ConfigureViewProtocol.swift
//  app-empresas
//
//  Created by João on 20/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import Foundation

protocol ConfigureView {
    func addViews()
    func addContraints()
    func setupView()
}

extension ConfigureView {
    func setupView() {
        addViews()
        addContraints()
    }
}
