//
//  ResponseFormattableProtocol.swift
//  app-empresas
//
//  Created by João on 21/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import Foundation

protocol ResponseFormattableProtocol {
    static func getModelFromData<T>(type: T.Type, _ data: Data) -> T? where T: Codable
}

extension ResponseFormattableProtocol {
    static func getModelFromData<T>(type: T.Type, _ data: Data) -> T? where T: Codable {
        do {
            let model = try JSONDecoder().decode(type, from: data)
            return model
        } catch let error as NSError {
            print(error.debugDescription)
            return nil
        }
    }
}
