//
//  LoginViewController.swift
//  app-empresas
//
//  Created by João on 20/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    private var viewModel: LoginViewModel
    private let uiView = LoginView()
    
    init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uiViewSetup()
        binding()
        hideKeyboardOnTapView()
        moveWhenKeyboardAppears()
    }
    
    override func loadView() {
        self.view = uiView
    }
    
    func uiViewSetup() {
        uiView.emailTextField.textField.delegate = self
        uiView.passwordTextField.textField.delegate = self
        uiView.passwordTextField.button.addTarget(self, action: #selector(showPassword), for: .touchUpInside)
        uiView.logInButton.addTarget(self, action: #selector(logInPressed), for: .touchUpInside)
    }
    
    func binding() {
        viewModel.succesLogin = { [weak self] in
            guard let self = self else { return }
            let viewModel = EnterprisesListViewModel()
            let controller = EnterprisesListViewController(viewModel: viewModel)
            controller.modalPresentationStyle = .fullScreen
            self.enabledLogInButton(true)
            self.present(controller, animated: true)
        }
        
        viewModel.failedLogin = { [weak self] in
            guard let self = self else { return }
            self.uiView.configureCredentialError()
            self.enabledLogInButton(true)
        }
    }
    
    func enabledLogInButton(_ enabled: Bool) {
        uiView.logInButton.isUserInteractionEnabled = enabled
        if enabled == false {
            uiView.logInButton.activityIndicator.startAnimating()
        } else {
            uiView.logInButton.activityIndicator.stopAnimating()
        }
    }
    
    @objc func logInPressed() {
        enabledLogInButton(false)
        view.endEditing(true)
        viewModel.makeLogin()
    }
    
    @objc func showPassword() {
        uiView.passwordTextField.textField.isSecureTextEntry = !uiView.passwordTextField.textField.isSecureTextEntry
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        logInPressed()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case uiView.emailTextField.textField:
            viewModel.email = textField.text
        case uiView.passwordTextField.textField:
            viewModel.password = textField.text
        default:
            break
        }
    }
}
