//
//  EnterprisesListViewController.swift
//  app-empresas
//
//  Created by João on 21/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit
import Kingfisher

class EnterprisesListViewController: UIViewController {
    
    private var viewModel: EnterprisesListViewModelProtocol
    private let uiView = EnterprisesListView()
    
    init(viewModel: EnterprisesListViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uiViewSetup()
        binding()
    }
    
    override func loadView() {
        self.view = uiView
    }

    func uiViewSetup() {
        uiView.searchTextField.delegate = self
        uiView.tableView.separatorStyle = .none
        uiView.tableView.delegate = self
        uiView.tableView.dataSource = self
        uiView.tableView.register(EnterpriseTableViewCell.self, forCellReuseIdentifier: EnterpriseTableViewCell.identifier)
        uiView.logoutButton.addTarget(self, action: #selector(logout), for: .touchUpInside)
    }
    
    func binding() {
        viewModel.succesGetEnterprises = { [weak self] numberOfResults in
            guard let self = self else { return }
            let text = numberOfResults == 0 ? "Nenhum resultado encontrado" : "\(numberOfResults) resultados encontrados"
            self.uiView.resultsFoundLabel.text = text
            self.uiView.tableView.reloadData()
        }
        
        viewModel.failedGetEnterprises = { [weak self] message in
            guard let self = self else { return }
            self.present(AlertHelper.errorAlert(message), animated: true)
        }
    }
    
    @objc func logout() {
        viewModel.logout { success in
            if success {
                self.dismiss(animated: true)
            } else {
                self.present(AlertHelper.errorAlert("Não foi possível sair da sessão"), animated: true)
            }
        }
    }
}

extension EnterprisesListViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        viewModel.getEnterprises(textField.text)
        return true
    }
}

extension EnterprisesListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ScreeSize.proportionallyHeight(0.2)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsTableView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EnterpriseTableViewCell.identifier, for: indexPath) as? EnterpriseTableViewCell else {
            return UITableViewCell()
        }
        let index = indexPath.row
        
        cell.nameLabel.text = viewModel.enterprises[index].enterpriseName
        cell.backImageView.kf.setImage(with: viewModel.enterprisePhoto(at: index))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = EnterpriseDetailViewModel(enterprise: self.viewModel.enterprises[indexPath.row])
        let controller = EnterpriseDetailViewController(viewModel: viewModel)
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true)
    }
}
