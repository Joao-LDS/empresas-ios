//
//  EnterpriseDetailViewController.swift
//  app-empresas
//
//  Created by João on 22/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import UIKit

class EnterpriseDetailViewController: UIViewController {
    
    private var viewModel: EnterpriseDetailViewModelProtocol
    private let uiView = EnterpriseDetailView()
    
    init(viewModel: EnterpriseDetailViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupView()
    }
    
    func configureView() {
        view.backgroundColor = .white
        uiView.nameLabel.text = viewModel.enterprise.enterpriseName
        uiView.detailLabel.text = viewModel.enterprise.detail
        uiView.imageView.kf.setImage(with: viewModel.enterprisePhoto())
        uiView.backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
    }
    
    @objc func backButtonPressed() {
        dismiss(animated: true)
    }
}

extension EnterpriseDetailViewController: ConfigureView {
    func addViews() {
        view.addSubview(uiView)
    }
    
    func addContraints() {
        uiView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        uiView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        uiView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        uiView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}
