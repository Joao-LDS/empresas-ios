//
//  EnterpriseResponse.swift
//  app-empresas
//
//  Created by João on 21/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import Foundation

struct EnterpriseResponse: Codable {
    let enterprises: [Enterprise]
    
    struct Enterprise: Codable {
        let id: Int
        let emailEnterprise, facebook, twitter, linkedin: String?
        let phone: String?
        let ownEnterprise: Bool
        let enterpriseName, photo, detail, city: String
        let country: String
        let value, sharePrice: Int
        let enterpriseType: EnterpriseType

        enum CodingKeys: String, CodingKey {
            case id
            case emailEnterprise = "email_enterprise"
            case facebook, twitter, linkedin, phone
            case ownEnterprise = "own_enterprise"
            case enterpriseName = "enterprise_name"
            case photo
            case detail = "description"
            case city, country, value
            case sharePrice = "share_price"
            case enterpriseType = "enterprise_type"
        }
    }
    
    struct EnterpriseType: Codable {
        let id: Int
        let enterpriseTypeName: String

        enum CodingKeys: String, CodingKey {
            case id
            case enterpriseTypeName = "enterprise_type_name"
        }
    }
}


