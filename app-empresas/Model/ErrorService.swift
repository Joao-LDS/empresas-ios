//
//  ErrorService.swift
//  app-empresas
//
//  Created by João on 21/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import Foundation

struct ErrorService: Error {
    let message: String
}
