//
//  Request.swift
//  app-empresas
//
//  Created by João on 21/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import Foundation
import Alamofire

class Request {
    static let path = "https://empresas.ioasys.com.br"
    static let apiVersion = "v1"
    
    static let sessionManager: Session = {
        let configuration = URLSessionConfiguration.af.default
        configuration.timeoutIntervalForRequest = 30
        configuration.waitsForConnectivity = true
        return Session(configuration: configuration)
    }()
    
    class Login {
        static let endPoint = "\(path)/api/\(apiVersion)/users/auth/sign_in"
    }
    
    class Enterprise {
        static let endPoint = "\(path)/api/\(apiVersion)/enterprises"
    }
}
