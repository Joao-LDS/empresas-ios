//
//  EnterprisesServiceRepository.swift
//  app-empresas
//
//  Created by João on 21/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import Foundation
import Alamofire

class EnterprisesServiceRepository: ResponseFormattableProtocol {
    
    static func getEnterprises(name: String, completion: @escaping((Result<EnterpriseResponse, ErrorService>)-> Void)) {
        let param: [String: String] = ["name": name]
        guard let header = Cache.getHeader() else {
            completion(.failure(ErrorService(message: "Não foi possível fazer a pesquisa.")))
            return
        }
        Request.sessionManager.request(
            Request.Enterprise.endPoint,
            method: .get,
            parameters: param,
            encoder: URLEncodedFormParameterEncoder.default,
            headers: HTTPHeaders(header)
        ).responseJSON { response in
            switch response.result {
            case .success:
                guard let data = response.data,
                    let enterprises = self.getModelFromData(type: EnterpriseResponse.self, data) else {
                        completion(.failure(ErrorService(message: "Não foi possível fazer a pesquisa.")))
                        return
                }
                completion(.success(enterprises))
            case .failure:
                print("")
            }
        }
    }
    
}
