//
//  LoginServiceRepository.swift
//  app-empresas
//
//  Created by João on 20/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import Foundation
import Alamofire

class LoginServiceRepository: ResponseFormattableProtocol {

    static func login(_ email: String,_ password: String, completion: @escaping (Result<[String:String]?, ErrorService>) -> Void) {
        let user = User(email: email, password: password)
        
        Request.sessionManager.request(Request.Login.endPoint,
                   method: .post,
                   parameters: user,
                   headers: ["Accept": "application/json"]
        ).response { response in
            if let error = response.error {
                completion(.failure(ErrorService(message: error.errorDescription ?? "Erro ao logar.")))
            } else {
                if let headers = response.response?.headers.dictionary,
                    response.response?.statusCode == 200 {
                    completion(.success(headers))
                } else {
                    completion(.failure(ErrorService(message: "Erro ao logar.")))
                }
            }
        }
    }
    
    static func logout(completion: @escaping(Bool) -> Void) {
        Cache.deleteHeader { success in
            completion(success)
        }
    }
}
