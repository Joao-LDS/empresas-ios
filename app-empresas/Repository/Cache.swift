//
//  Cache.swift
//  app-empresas
//
//  Created by João on 22/05/21.
//  Copyright © 2021 João. All rights reserved.
//

import Foundation

class Cache {
    
    static func saveHeader(_ dict: [String:String]?) {
        guard let header = dict,
            let accessToken = header["access-token"],
            let uid = header["uid"],
            let client = header["client"] else { return }
        
        let userDefaults = UserDefaults.standard
        userDefaults.setValue(accessToken, forKeyPath: "access-token")
        userDefaults.setValue(uid, forKeyPath: "uid")
        userDefaults.setValue(client, forKeyPath: "client")
    }
    
    static func deleteHeader(completion: @escaping(Bool) -> Void) {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "access-token")
        userDefaults.removeObject(forKey: "uid")
        userDefaults.removeObject(forKey: "client")
        
        if userDefaults.value(forKey: "access-token") == nil,
            userDefaults.value(forKey: "uid") == nil,
            userDefaults.value(forKey: "client") == nil {
            completion(true)
        }
        completion(false)
    }
    
    static func getHeader() -> [String:String]? {
        let userDefaults = UserDefaults.standard
        guard let accessToke = userDefaults.value(forKey: "access-token") as? String,
            let uid = userDefaults.value(forKey: "uid") as? String,
            let client = userDefaults.value(forKey: "client") as? String else {
                return nil
        }
        return ["access-token": accessToke,
                "uid": uid,
                "client":  client]
    }
    
}
